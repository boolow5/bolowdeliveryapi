package router

import (
	"net/http"

	"bitbucket.org/boolow5/bolowdeliveryapi/controllers"
	"bitbucket.org/boolow5/bolowdeliveryapi/middlewares"
	"bitbucket.org/boolow5/bolowdeliveryapi/models"
	"github.com/gin-gonic/gin"
)

func setupV1(router *gin.Engine) {

	// routes for everyone, even non-logged in
	v1 := router.Group("/api/v1")
	{
		v1.GET("/version", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"version": models.Version,
				"type":    "v1",
				"build":   models.CommitNo,
				"date":    models.LastBuidDate,
				"domain":  c.Request.Host,
			})
		})
		v1.GET("/ping", controllers.Ping)
		v1.POST("/signup", controllers.Signup)
		v1.POST("/login", controllers.Login)

		v1.GET("/categories", controllers.GetCategories)
	}
	authorized := v1.Group("/")

	// routes for all logged in users
	authorized.Use(middlewares.Auth())
	{
		// general endpoints
		authorized.GET("/user", controllers.GetCurrentUser)
		authorized.GET("/profile", controllers.GetProfile)
		authorized.GET("/user-level", controllers.GetUserLevel)
		authorized.GET("/notifications", controllers.GetMyNotifications)

		// vendor endpoints
		authorized.GET("/my-shops", controllers.GetMyShops)
		authorized.PUT("/my-shops/:id", controllers.UpdateShop)
		authorized.GET("/my-shops/:id/products", controllers.GetMyProducts)
		authorized.POST("/my-shops/:id/products", controllers.CreateProduct)
		authorized.GET("/category-items/:categoryName", controllers.GetProductsByCategoryName)

		// customer orders
		authorized.GET("/nearby-shops", controllers.GetNearbyShops)
		authorized.GET("/vendors/:id", controllers.GetShopInfo)
		authorized.GET("/vendors/:id/products", controllers.GetShopProducts)
		authorized.GET("/orders", controllers.GetMyOrders)
		authorized.POST("/orders", controllers.AddOrder)
		authorized.PUT("/orders/:id/cancel", controllers.CancelOrder)
		authorized.PUT("/orders/:id/undo-cancel", controllers.UndoCancelOrder)
		authorized.DELETE("/orders/:id/items", controllers.RemoveItemFromOrder)

		// vendor order approval
		authorized.PUT("/orders/:id/approve", controllers.ApproveOrder)
	}
}
