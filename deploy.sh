#!/bin/bash

export HOST="192.168.0.1"
# Package name
export API_PACKAGE=bolowdeliveryapi.tar.gz
# The remote directory of the server
export DEPLOY_PATH=/path/in/your/server
# static files directory
export STATIC_PATH=/path/in/your/server
# remote server user
export OWNER=mahdi
# web user that runs the web app
export WEB_USER=bolowdeliveryapi
# process name
export PROC_NAME=bolowdeliveryapi

scp $API_PACKAGE ${OWNER}@${HOST}:/tmp

echo "sending \"$API_PACKAGE\" to \"$DEPLOY_PATH/bin\""

ssh -t ${OWNER}@${HOST} "
  sudo rm -r $DEPLOY_PATH/bin &&
  sudo tar -xvzf /tmp/$API_PACKAGE -C $DEPLOY_PATH/;
  sudo ln -s $STATIC_PATH $DEPLOY_PATH/bin/static;
  sudo find $DEPLOY_PATH/bin -type d -exec chmod 755 {} \;
  sudo find $DEPLOY_PATH/bin -type f -exec chmod 754 {} \;
  sudo chown -R $WEB_USER $DEPLOY_PATH/bin;
  sudo supervisorctl restart ${PROC_NAME} || true
";
