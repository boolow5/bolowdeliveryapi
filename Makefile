PKGNAME := bolowdeliveryapi.tar.gz
COMMIT_NO := $(shell git rev-parse --short HEAD)
BUID_DATA := $(shell date -u +%Y-%m-%d_%H:%M:%S_%Z)

build: linux-build package

linux-build:
	GOOS=linux GOARCH=amd64 go build -v -ldflags "-X bitbucket.org/boolow5/bolowdeliveryapi/models.CommitNo=${COMMIT_NO} -X bitbucket.org/boolow5/bolowdeliveryapi/models.LastBuidDate=${BUID_DATA}" -o bin/bolowdeliveryapi

clean:
	rm -r bin/*

init:
	mkdir -p bin log
	go get -v -u ./...

package:
	@mv bin old_bin;
	@mkdir -p bin;
	@mkdir -p bin/templates
	@cp old_bin/bolowdeliveryapi bin/;
	@cp config.json bin/;
	@cp templates/* bin/templates
	@cp -r data bin/;

	@tar -zcf ${PKGNAME} bin/;
	@rm -r bin;
	@mv old_bin bin;
	@echo "Package ready!"

# make deploy deploys test api
deploy:
	@bash deploy.sh test
	@echo "Deployed test successfully!"

# while make deploy-prod deploys production API
deploy-prod: # production
	@bash deploy.sh prod
	@echo "Deployed production successfully!"

# it's same as running 'make deploy && make deploy prod
deploy-both: deploy deploy-prod
