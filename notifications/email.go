package notifications

import (
	"bytes"
	"fmt"
	"html/template"
	"path/filepath"

	"bitbucket.org/boolow5/bolowdeliveryapi/config"
	"bitbucket.org/boolow5/bolowdeliveryapi/utils"
	"gopkg.in/mailgun/mailgun-go.v1"
)

const (
	// EmailMsgStatusNotSent means email was created but not yet sent
	EmailMsgStatusNotSent = "not_sent"
	// EmailMsgStatusSent means email was sent
	EmailMsgStatusSent = "sent"
	// EmailMsgStatusFailed means sending failed
	EmailMsgStatusFailed = "failed"
)

const (
	MsgTypeGeneric = iota
	MsgTypeSentPayment
	MsgTypeReceivedPayment
	MsgTypeReloadWallet
	MsgTypeDriverApproved
)

const (
	// NoReplyEmail is the email used for messages that does not allow user to reply to
	NoReplyEmail = "noreply@bolowdeliveryapi.com"
	// InfoEmail is the email used for most notifications
	InfoEmail = "info@bolowdeliveryapi.com"
	// AdminEmail is the owner email which is different from the rest of emails
	AdminEmail = "boolow5@gmail.com"
)

var (
	// MailGunInstance represents mailgun instance used to send emails
	MailGunInstance mailgun.Mailgun
)

// EmailMessage represents an email message
type EmailMessage struct {
	Subject    string        `json:"subject"`
	Sender     string        `json:"sender"`
	Recipients []string      `json:"recipients"`
	Body       []byte        `json:"body"`
	Response   EmailResponse `json:"response"`
	Status     string        `json:"status"`
}

// MgResponse wraps around the returned data from sent email
type MgResponse struct {
	ID    string
	Msg   string
	Error error
}

// DataItem is key value pair
type DataItem struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// EmailResponse represents data sent to email template for rendeding
type EmailResponse struct {
	Title       string
	Message     string
	DataList    []DataItem
	Amount      string
	Description string
	MsgType     int
}

// NewMailResponse initializes MailResponse object
func NewMailResponse(title, msg, amount, description string, dataMap map[string]string, msgType int) EmailResponse {
	var dataList []DataItem
	for key, val := range dataMap {
		dataList = append(dataList, DataItem{key, val})
	}

	return EmailResponse{
		Title:       title,
		Message:     msg,
		DataList:    dataList,
		Amount:      amount,
		Description: description,
		MsgType:     msgType,
	}
}

// NewEmail initializes an EmailMessage object and returns
func NewEmail(subject, sender string, response EmailResponse, recipients ...string) EmailMessage {
	if !utils.IsValidEmail(sender) {
		sender = "info@bolowdeliveryapi.com"
	}

	body, _ := response.GetBody()

	return EmailMessage{
		Subject:    subject,
		Sender:     sender,
		Body:       body,
		Response:   response,
		Recipients: recipients,
		Status:     EmailMsgStatusNotSent,
	}
}

// InitMailGun initializes main gun
func InitMailGun(conf config.MailGunConfig) {
	MailGunInstance = mailgun.NewMailgun(conf.DomainKey, conf.APIKey, conf.APIKey)
}

// Send sends EmailMessage through MailGunInstance
func (em EmailMessage) Send() MgResponse {
	if len(em.Recipients) == 0 {
		return MgResponse{ID: "", Msg: "", Error: fmt.Errorf("no recipients")}
	}
	body, err := em.Response.GetBody()
	if err != nil {
		return MgResponse{
			ID:    "",
			Msg:   err.Error(),
			Error: err,
		}
	}
	msg := mailgun.NewMessage(em.Sender, em.Subject, string(body), em.Recipients...)
	msg.SetHtml(string(body))
	resp, id, err := MailGunInstance.Send(msg)
	fmt.Printf("\nSEND: from '%s' to '%v'\n", em.Sender, em.Recipients)
	return MgResponse{
		ID:    id,
		Msg:   resp,
		Error: err,
	}
}

// GetBody parses EmailResponse and renders the template and sets response body
func (e EmailResponse) GetBody() ([]byte, error) {
	var body []byte
	var err error

	switch e.MsgType {
	case MsgTypeGeneric:
		body, err = ParseTemplate("generic", e)
	case MsgTypeSentPayment:
		body, err = ParseTemplate("payment-sent", e)
	case MsgTypeReceivedPayment:
		body, err = ParseTemplate("payment-received", e)
	case MsgTypeReloadWallet:
		body, err = ParseTemplate("reload", e)
	case MsgTypeDriverApproved:
		body, err = ParseTemplate("driver-approved", e)
	}
	return body, err
}

// ParseTemplate takes email template file name and renders content of the response
func ParseTemplate(name string, response EmailResponse) ([]byte, error) {
	dir := config.GetPath()
	fullPath := filepath.Join(dir, "templates", name+".html")
	t := template.Must(template.ParseFiles(fullPath))
	output := &bytes.Buffer{}
	err := t.Execute(output, response)
	return output.Bytes(), err
}
