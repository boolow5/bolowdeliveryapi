package db

import (
	// _ "github.com/mattn/go-sqlite3"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// // TableField represents a field that should be used to create SQL table or in insert
// type TableField struct {
// 	Name     string
// 	DataType string
// 	Default  string
// 	IsNull   bool
// 	Value    interface{}
// }

// // ToTableField fields for a SQL table generation
// func ToTableField(v reflect.Value, t reflect.StructField) TableField {
// 	tagInfo := t.Tag.Get("db")
// 	tagName := t.Tag.Get("json")

// 	isNull := false
// 	if len(strings.TrimSpace(tagName)) == 0 {
// 		tagName = t.Name
// 	}

// 	if strings.Contains(tagInfo, "null") || strings.Contains(tagInfo, "NULL") {
// 		tagInfo = strings.ReplaceAll(tagInfo, ",null", "")
// 		tagInfo = strings.ReplaceAll(tagInfo, ", null", "")
// 		tagInfo = strings.ReplaceAll(tagInfo, "null,", "")

// 		tagInfo = strings.ReplaceAll(tagInfo, ",NULL", "")
// 		tagInfo = strings.ReplaceAll(tagInfo, ", NULL", "")
// 		tagInfo = strings.ReplaceAll(tagInfo, "NULL,", "")
// 		isNull = true
// 	}

// 	dtype := toDBType(fmt.Sprintf("%s", v.Type))

// 	var value interface{}
// 	if v.IsValid() {
// 		value = v
// 	}

// 	return TableField{
// 		Name:     tagName,
// 		DataType: dtype,
// 		IsNull:   isNull,
// 		Value:    value,
// 	}
// }

// // GetFieldValues returns fields from struct as TableField slice
// func GetFieldValues(item interface{}) []TableField {
// 	fields := make([]TableField, 0)
// 	var ifv reflect.Value
// 	var ift reflect.Type

// 	if reflect.TypeOf(item).Kind() == reflect.Ptr {
// 		ifv = reflect.Indirect(reflect.ValueOf(item))
// 		ift = ifv.Type()
// 	} else {
// 		ifv = reflect.ValueOf(item)
// 		ift = reflect.TypeOf(item)
// 	}

// 	fmt.Printf("[GetFieldValues] is nil %v\tifv: %v\tift: %v\n", item == nil, ifv.Kind(), ift.Kind())

// 	for i := 0; i < ift.NumField(); i++ {
// 		v := ifv.Field(i)

// 		if !v.CanInterface() {
// 			fmt.Println("Skipping field:", ift.Field(i).Name)
// 			continue
// 		}

// 		switch v.Kind() {
// 		case reflect.Struct:
// 			fmt.Println("\nParsing struct:", ift.Field(i).Name, "\n", ift.Field(i).Type)
// 			if fmt.Sprint(ift.Field(i).Type) == "time.Time" {
// 				fmt.Println("Is time Object .....................")
// 				tagInfo := ift.Field(i).Tag.Get("db")
// 				tagName := ift.Field(i).Tag.Get("json")

// 				if len(strings.TrimSpace(tagName)) == 0 {
// 					tagName = ift.Field(i).Name
// 				}

// 				isNull := strings.Contains(tagInfo, "null") || strings.Contains(tagInfo, "NULL")
// 				Default := ""
// 				if strings.Contains(tagInfo, "default") || strings.Contains(tagInfo, "DEFAULT") {
// 					parts := strings.Split(tagInfo, ",")
// 					for i := 0; i < len(parts); i++ {
// 						if strings.HasPrefix(parts[i], "default") || strings.HasPrefix(parts[i], "DEFAULT") {
// 							p := strings.Split(parts[i], "=")
// 							if len(p) > 1 {
// 								Default = p[1]
// 							}
// 						}
// 					}
// 				}
// 				fields = append(fields, TableField{
// 					Name:     tagName,
// 					DataType: toDBType(fmt.Sprintf("%s", ifv.Field(i).Type())),
// 					IsNull:   isNull,
// 					Default:  Default,
// 					Value:    ifv.Field(i),
// 				})
// 			} else {
// 				fields = append(fields, GetFieldValues(v.Interface())...)
// 			}
// 		default:
// 			fields = append(fields, ToTableField(v, ift.Field(i)))
// 		}
// 	}

// 	return fields
// }

// func toDBType(t string) string {
// 	switch t {
// 	case "int", "int8", "int16", "int32", "int64", "uint", "uint8", "uint16", "uint32", "uint64":
// 		return "NUMERIC"
// 	case "float", "float8", "float16", "float32":
// 		return "NUMERIC"
// 	case "float64":
// 		return "NUMERIC(24, 8)"
// 	case "time.Time":
// 		return "TIMESTAMP WITH TIME ZONE"
// 	default:
// 		return "text"
// 	}
// }

// // IsZeroType returns if a x has any value of zero
// func IsZeroType(x interface{}) bool {
// 	return reflect.DeepEqual(x, reflect.Zero(reflect.TypeOf(x)).Interface())
// }

// // StructToTable creates SQL string to create a table for the passed interface
// func StructToTable(v interface{}) string {
// 	tableName := strings.ToLower(reflect.TypeOf(v).Elem().Name())
// 	fields := GetFieldValues(v)
// 	innerFields := []string{}

// 	if len(fields) == 0 {
// 		return ""
// 	}

// 	for _, field := range fields {
// 		extra := ""
// 		if field.IsNull {
// 			extra += " NULL"
// 		} else {
// 			extra += " NOT NULL"
// 		}
// 		if field.Default != "" {
// 			extra += fmt.Sprintf(" DEFAULT '%s'", field.Default)
// 		}
// 		innerFields = append(innerFields, fmt.Sprintf(`
// 		%s %s %s`, field.Name, field.DataType, extra))
// 	}
// 	return fmt.Sprintf(`
// CREATE TABLE IF NOT EXISTS %s (
// 	%s
// );
// 	`, tableName, strings.Join(innerFields, ","))
// }

// // Register creates a table if not already exists
// func Register(v interface{}) error {
// 	table := StructToTable(v)
// 	if len(table) == 0 {
// 		return fmt.Errorf("invalid table")
// 	}
// 	if !strings.HasPrefix(table, "\nCREATE TABLE IF NOT EXISTS") {
// 		fmt.Println("Table: \n", table)
// 		return fmt.Errorf("invalid table query")
// 	}
// 	fmt.Println("Table: \n", table)
// 	DB.MustExec(table).RowsAffected()
// 	return nil
// }

// // StructToTableInsert creates SQL string to insert data in the passed interface
// func StructToTableInsert(v interface{}) (string, []interface{}) {
// 	tableName := strings.ToLower(reflect.TypeOf(v).Elem().Name())
// 	fields := GetFieldValues(v)
// 	fieldNames := []string{}
// 	fieldValues := []interface{}{}

// 	for _, field := range fields {
// 		fieldNames = append(fieldNames, field.Name)
// 		val := field.Value

// 		if IsZeroType(val) && field.Default != "" {
// 			val = field.Default
// 		}

// 		valStr := fmt.Sprintf("%v", val)

// 		if strings.Contains(valStr, "ObjectIdHex(") {
// 			fmt.Printf("\tFieldName: %s, valStr: %v\n", field.Name, valStr)
// 			valStr = valStr[:len(valStr)-2]
// 			valStr = strings.Replace(valStr, "ObjectIdHex(\"", "", -1)
// 		}

// 		fieldValues = append(fieldValues, valStr)
// 	}

// 	fieldPlaceHolders := []string{}
// 	for i := 0; i < len(fieldNames); i++ {
// 		fieldPlaceHolders = append(fieldPlaceHolders, "?")
// 	}

// 	return fmt.Sprintf(`
// 	INSERT INTO %s (%s)
// 	VALUES (%s)
// 	`, tableName, strings.Join(fieldNames, ", "), strings.Join(fieldPlaceHolders, ", ")), fieldValues
// }

// // StructToTableUpdate creates SQL string to insert data in the passed interface
// func StructToTableUpdate(v interface{}, updateFields ...string) (string, []interface{}) {
// 	tableName := strings.ToLower(reflect.TypeOf(v).Elem().Name())
// 	fields := GetFieldValues(v)
// 	fieldNames := []string{}
// 	fieldValues := []interface{}{}

// 	for _, field := range fields {
// 		for j := 0; j < len(updateFields); j++ {
// 			if field.Name == updateFields[j] {
// 				fieldNames = append(fieldNames, field.Name)
// 				val := field.Value

// 				if IsZeroType(val) && field.Default != "" {
// 					val = field.Default
// 				}

// 				valStr := fmt.Sprintf("%v", val)

// 				if strings.Contains(valStr, "ObjectIdHex(") {
// 					fmt.Printf("\tFieldName: %s, valStr: %v\n", field.Name, valStr)
// 					valStr = valStr[:len(valStr)-2]
// 					valStr = strings.Replace(valStr, "ObjectIdHex(\"", "", -1)
// 				}

// 				fieldValues = append(fieldValues, valStr)
// 			}
// 		}
// 	}

// 	fieldPlaceHolders := []string{}
// 	for i := 0; i < len(fieldNames); i++ {
// 		fieldPlaceHolders = append(fieldPlaceHolders, fmt.Sprintf("%s = ?", fieldNames[i]))
// 	}

// 	return fmt.Sprintf(`
// 	UPDATE %s
// 	SET %s
// 	`, tableName, strings.Join(fieldPlaceHolders, ", ")), fieldValues
// }

// // SelectByIDQuery generates SQL for selecting stuct by id
// func SelectByIDQuery(v interface{}) (query, id string) {
// 	fields := GetFieldValues(v)
// 	fieldNames := []string{}

// 	for _, field := range fields {
// 		fieldNames = append(fieldNames, field.Name)
// 		if len(id) == 0 && field.Name == "id" {
// 			valStr := fmt.Sprintf("%v", field.Value)
// 			if strings.Contains(valStr, "ObjectIdHex(") {
// 				fmt.Printf("\tFieldName: %s, valStr: %v\n", field.Name, valStr)
// 				valStr = valStr[:len(valStr)-2]
// 				valStr = strings.Replace(valStr, "ObjectIdHex(\"", "", -1)
// 			}
// 			id = valStr
// 		}
// 	}
// 	return fmt.Sprintf(`SELECT %s FROM %s WHERE id = ?`, strings.Join(fieldNames, ", "), strings.ToLower(reflect.TypeOf(v).Elem().Name())), id
// }
