package db

// var (
// 	// DB connection pool
// 	DB *sqlx.DB
// )

// // SQLObject defines structs that can be updated by below CRUD functions
// type SQLObject interface {
// 	UpdateFromMap(m map[string]interface{}) (bool, error)
// }

// // InitPostgres initialzes postgres database connection
// func InitPostgres() {
// 	conf := config.Get()
// 	fmt.Printf("[SQL] connecting to database '%s'\n", conf.Postgres.DBName)
// 	var err error
// 	DB, err = sqlx.Connect("postgres", conf.Postgres.URL())
// 	if err != nil {
// 		panic("[SQL] ERROR: " + err.Error())
// 	}

// 	DB.SetMaxOpenConns(1000) // The default is 0 (unlimited)
// 	DB.SetMaxIdleConns(10)   // defaultMaxIdleConns = 2
// 	DB.SetConnMaxLifetime(0) // 0, connections are reused forever.
// 	fmt.Printf("[SQL] Connected successfully!\nStats: %+v\n", DB.Stats())
// }

// // SQLTransaction creates a session
// func SQLTransaction() *sqlx.Tx {
// 	return DB.MustBegin()
// }

// // CreateTable creates a table
// func CreateTable(v interface{}) error {
// 	schema := StructToTable(v)
// 	DB.MustExec(schema)
// 	return nil
// }

// // InsertToTable inserts values to database table
// func InsertToTable(v interface{}) sql.Result {
// 	query, values := StructToTableInsert(v)
// 	return DB.MustExec(query, values...)
// }

// // UpdateToTable updates values to database table
// func UpdateToTable(v interface{}, fields []string) sql.Result {
// 	query, values := StructToTableUpdate(v, fields...)
// 	return DB.MustExec(query, values...)
// }

// // GetTableDataByID finds a table entry by ID
// func GetTableDataByID(so SQLObject) error {
// 	query, id := SelectByIDQuery(so)
// 	var m map[string]interface{}
// 	err := DB.Get(&m, query, id)
// 	if err != nil {
// 		return err
// 	}
// 	updated, err := so.UpdateFromMap(m)
// 	if err != nil {
// 		return err
// 	}
// 	if !updated {
// 		return fmt.Errorf("updatiing the output object failed")
// 	}
// 	return nil
// }
