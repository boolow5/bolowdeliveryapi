package controllers

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"bitbucket.org/boolow5/bolowdeliveryapi/models"
	"bitbucket.org/boolow5/bolowdeliveryapi/notifications"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// Ping is used to test sse channel is working
func Ping(c *gin.Context) {
	channel := c.Query("channel")
	msg := []byte(fmt.Sprintf("[%s]\tPONG!", time.Now().Format(time.ANSIC)))
	targets := []string{"sse"}
	options := map[string]interface{}{
		"channel": channel,
	}
	err := notifications.Send(msg, targets, options)
	if err != nil {
		logger.Error("failed to send PONG message:", err)
		AbortWithError(c, http.StatusBadRequest, NewError(UknownError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, successResponse)
}

// GetMyNotifications returns current customer's notifications
func GetMyNotifications(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	/* customer */ _, err := models.GetCustomerByID(userID)

	if err != nil {
		logger.Errorf("Error finding customer: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	notifications, err := models.GetNotificationsByUserID(userID)
	if err != nil {
		logger.Errorf("Error finding notifications: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"notifications": notifications})
}
