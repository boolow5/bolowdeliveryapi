package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"bitbucket.org/boolow5/bolowdeliveryapi/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// GetMyProducts finds products of a shop
func GetMyProducts(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	shopID := bson.ObjectIdHex(c.Param("id"))

	vendor, err := models.GetVendorByUserID(userID)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}

	found := false

	for _, shop := range vendor.Shops {
		if shop.ID == shopID {
			found = true
		}
	}

	if !found {
		logger.Errorf("Access error: shop does not belong to you ")
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, "Access denied: shop does not belong to you"))
		return
	}

	products, err := models.GetProductsByShopID(shopID)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"products": products})
}

// GetShopProducts finds products of a shop
func GetShopProducts(c *gin.Context) {
	shopID := bson.ObjectIdHex(c.Param("id"))
	products, err := models.GetProductsByShopID(shopID)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"products": products})
}

// CreateProduct saves a product for current shop
func CreateProduct(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	shopID := bson.ObjectIdHex(c.Param("id"))

	vendor, err := models.GetVendorByUserID(userID)
	if err != nil {
		logger.Errorf("Error finding vendor: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	found := false

	for _, shop := range vendor.Shops {
		if shop.ID == shopID {
			found = true
			break
		}
	}

	if !found {
		logger.Errorf("Access error: shop doesn't belong to you ", err)
		AbortWithError(c, http.StatusForbidden, NewError(PermissionError, "shop doesn't belong to you"))
		return
	}

	shop, err := models.GetShopByID(shopID)
	if err != nil {
		logger.Errorf("Error finding shop: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	input := models.Product{
		Shop: shop,
	}

	err = c.Bind(&input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}

	errs := models.Save(&input)
	if errs != nil && len(errs) > 0 {
		logger.Error("saving product failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "Success"})
}

// GetProductsByCategoryName finds items by their category name
func GetProductsByCategoryName(c *gin.Context) {
	categoryName := c.Param("categoryName")

	items, err := models.GetProductsByCategoryName(categoryName)
	if err != nil {
		logger.Error("fetching products failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(SavingItemError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"items": items})
}
