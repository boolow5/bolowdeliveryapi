package controllers

import (
	"net/http"
	"strconv"

	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"bitbucket.org/boolow5/bolowdeliveryapi/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// GetMyShops finds current user's shops
func GetMyShops(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))

	vendor, err := models.GetVendorByUserID(userID)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"shops": vendor.Shops})
}

// UpdateShop saves changes for current user's shops
func UpdateShop(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	shopID := bson.ObjectIdHex(c.Param("id"))

	input := models.Shop{}

	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}

	err = models.UpdateShop(shopID, userID, input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(SavingItemError, err.Error()))
		return
	}

	shop, err := models.GetShopByID(shopID)
	if err != nil {
		logger.Errorf("Error finding shop: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(SavingItemError, err.Error()))
		return
	}

	vendor, err := models.GetVendorByID(input.OwnerID)
	if err != nil {
		logger.Errorf("Error finding vendor: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(SavingItemError, err.Error()))
		return
	}

	for i := 0; i < len(vendor.Shops); i++ {
		if vendor.Shops[i].ID == shopID {
			vendor.Shops[i] = shop
		}
	}

	errs := models.Update(&vendor)
	if errs != nil && len(errs) > 0 {
		logger.Error("updating vendor failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	products, err := models.GetProductsByShopID(shopID)
	if err != nil {
		logger.Errorf("Error finding products: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(SavingItemError, err.Error()))
		return
	}

	productPtrs := make([]models.DBObject, len(products))

	for i := 0; i < len(products); i++ {
		products[i].Shop = shop
		productPtrs[i] = &products[i]
	}

	errs = models.UpdateAll(productPtrs...)
	if errs != nil && len(errs) > 0 {
		logger.Error("updating products failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "Success"})
}

// GetShopInfo finds a shop by its ID
func GetShopInfo(c *gin.Context) {
	shopID := bson.ObjectIdHex(c.Param("id"))

	shop, err := models.GetShopByID(shopID)
	if err != nil {
		logger.Errorf("Error finding shop: %v ", err)
		AbortWithError(c, http.StatusExpectationFailed, NewError(DataFetchingError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"shop": shop})
}

// GetNearbyShops finds nearby shops depending on user's current location
func GetNearbyShops(c *gin.Context) {
	latStr := c.Query("lat")
	lngStr := c.Query("lng")
	radiusStr := c.Query("radius")

	lat, err := strconv.ParseFloat(latStr, 64)
	if err != nil {
		logger.Errorf("Error parsing latitude: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	lng, err := strconv.ParseFloat(lngStr, 64)
	if err != nil {
		logger.Errorf("Error parsing longitude: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	radius, err := strconv.ParseFloat(radiusStr, 64)
	if err != nil {
		logger.Errorf("Error parsing radius: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}

	shops, err := models.GetNearbyShops(models.Location{Lat: lat, Long: lng}, nil, "", radius)
	if err != nil {
		logger.Errorf("Error finding shops: %v ", err)
		AbortWithError(c, http.StatusInternalServerError, NewError(DataFetchingError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"shops": shops})
}
