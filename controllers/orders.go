package controllers

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"bitbucket.org/boolow5/bolowdeliveryapi/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// AddOrder creates new order
func AddOrder(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	customer, err := models.GetCustomerByID(userID)

	if err != nil {
		logger.Errorf("Error finding customer: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	type item struct {
		ID            bson.ObjectId `json:"id"`
		OrderQuantity int64         `json:"order_quantity"`
	}

	input := []item{}

	err = c.Bind(&input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}

	itemIDs := []bson.ObjectId{}

	for _, item := range input {
		itemIDs = append(itemIDs, item.ID)
	}

	items, err := models.GetProductsByID(itemIDs)
	if err != nil {
		logger.Errorf("Error finding products: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	for i := 0; i < len(items); i++ {
		for j := 0; j < len(input); j++ {
			if items[i].ID == input[j].ID {
				items[i].OrderQuantity = input[j].OrderQuantity
			}
		}
	}

	order := models.Order{
		Customer: customer,
		Items:    items,
		Status:   models.OrderStatusPending,
	}

	errs := models.Save(&order)
	if errs != nil && len(errs) > 0 {
		logger.Error("saving user failed: ", errs)
		AbortWithErrors(c, http.StatusNotModified, NewErrors(SavingItemError, errs))
		return
	}

	msg := fmt.Sprintf("a customer added an order for %d items", len(items))
	targetUsers := []bson.ObjectId{}
	owners, err := items[0].Shop.GetOwners()
	if err != nil {
		logger.Errorf("Error finding products: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	fmt.Printf("\nOWNERS: %d\n\t%+v\n", len(owners), owners)

	for _, person := range owners {
		//
		targetUsers = append(targetUsers, person.ID)
	}

	err = order.Notify("create", msg, "/customer-orders", customer.Profile, targetUsers)
	if err != nil {
		logger.Errorf("Error finding products: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"order": order})
}

// RemoveItemFromOrder creates new order
func RemoveItemFromOrder(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	customer, err := models.GetCustomerByID(userID)

	if err != nil {
		logger.Errorf("Error finding customer: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}

	orderID := bson.ObjectIdHex(c.Param("id"))
	itemsStr := strings.Split(c.Query("items"), ",")
	IDs := []bson.ObjectId{}

	for _, id := range itemsStr {
		if bson.IsObjectIdHex(id) {
			IDs = append(IDs, bson.ObjectIdHex(id))
		}
	}

	order := models.Order{ID: orderID}
	err = models.GetItemByID(orderID, &order)
	if err != nil {
		logger.Errorf("Error finding item: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}

	list := []string{
		models.OrderStatusDelivering,
		models.OrderStatusReceived,
		models.OrderStatusCancelled,
		models.OrderStatusRejected,
	}

	if strings.Contains(strings.Join(list, ","), order.Status) {
		logger.Errorf("Cannot cancel or remove items from order with %s status", order.Status)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, fmt.Sprintf("Cannot cancel or remove items from order with %s status", order.Status)))
		return
	}

	removedItems := []string{}

	type ownerData struct {
		OwnerID bson.ObjectId
		Items   []models.Product
	}

	owners := map[string]ownerData{}

	for _, item := range order.Items {
		// ownersIDs = append(ownersIDs, item.Shop.OwnerID)
		if _, found := owners[item.Shop.OwnerID.Hex()]; !found {
			owners[item.Shop.OwnerID.Hex()] = ownerData{
				OwnerID: item.Shop.OwnerID,
				Items:   []models.Product{item},
			}
		} else {
			owners[item.Shop.OwnerID.Hex()] = ownerData{
				OwnerID: item.Shop.OwnerID,
				Items:   append(owners[item.Shop.OwnerID.Hex()].Items, item),
			}
		}
	}
	// , err := order.Items[0].Shop.GetOwners()
	if err != nil {
		logger.Errorf("Error finding shop owners: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	fmt.Printf("\nOWNERS: %d\n\t%+v\n", len(owners), owners)
	for i := len(order.Items) - 1; i >= 0; i-- {
		for _, id := range IDs {
			if order.Items[i].ID == id {
				removedItems = append(removedItems, order.Items[i].Name)
				order.Items = append(order.Items[:i], order.Items[i+1:]...)
			}
		}
	}

	errs := []error{}

	if len(order.Items) == 0 {
		err = models.Remove(&order)
		if err != nil {
			errs = []error{err}
		}
	} else {
		errs = models.Update(&order)
	}

	if errs != nil && len(errs) > 0 {
		logger.Error("saving order failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	targetUsers := []bson.ObjectId{}
	for _, owner := range owners {
		targetUsers = append(targetUsers, owner.OwnerID)
	}

	msg := fmt.Sprintf("Customer removed %d items: (%s)", len(removedItems), strings.Join(removedItems, ", "))

	err = order.Notify("create", msg, "/customer-orders", customer.Profile, targetUsers)
	if err != nil {
		logger.Errorf("Error sending notification: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "Success"})
}

// CancelOrder creates new order
func CancelOrder(c *gin.Context) {
	time.Sleep(10 * time.Second)
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	/* customer */ _, err := models.GetCustomerByID(userID)

	if err != nil {
		logger.Errorf("Error finding customer: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}

	orderID := bson.ObjectIdHex(c.Param("id"))

	order := models.Order{ID: orderID}
	err = models.GetItemByID(orderID, &order)
	if err != nil {
		logger.Errorf("Error finding item: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}

	order.Status = models.OrderStatusCancelled

	errs := models.Update(&order)
	if errs != nil && len(errs) > 0 {
		logger.Error("saving order failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "Success"})
}

// UndoCancelOrder to reverse cancelling
func UndoCancelOrder(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	/* customer */ _, err := models.GetCustomerByID(userID)

	if err != nil {
		logger.Errorf("Error finding customer: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}

	orderID := bson.ObjectIdHex(c.Param("id"))

	order := models.Order{ID: orderID}
	err = models.GetItemByID(orderID, &order)
	if err != nil {
		logger.Errorf("Error finding item: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}

	order.Status = models.OrderStatusPending

	errs := models.Update(&order)
	if errs != nil && len(errs) > 0 {
		logger.Error("saving order failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "Success"})
}

// ApproveOrder creates new order
func ApproveOrder(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	/* customer */ _, err := models.GetCustomerByID(userID)

	if err != nil {
		logger.Errorf("Error finding customer: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}

	orderID := bson.ObjectIdHex(c.Param("id"))

	order := models.Order{ID: orderID}
	err = models.GetItemByID(orderID, &order)
	if err != nil {
		logger.Errorf("Error finding item: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}

	order.Status = models.OrderStatusVerified

	errs := models.Update(&order)
	if errs != nil && len(errs) > 0 {
		logger.Error("saving user failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "Success"})
}

// GetMyOrders returns current customer's orders
func GetMyOrders(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	/* customer */ _, err := models.GetCustomerByID(userID)

	if err != nil {
		logger.Errorf("Error finding customer: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	orders, err := models.GetOrdersByUserID(userID)
	if err != nil {
		logger.Errorf("Error finding orders: %v ", err)
		AbortWithError(c, http.StatusNotFound, NewError(DataFetchingError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"orders": orders})
}
