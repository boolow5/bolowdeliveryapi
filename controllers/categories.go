package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"bitbucket.org/boolow5/bolowdeliveryapi/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// GetCategories finds all categories
func GetCategories(c *gin.Context) {
	categories, err := models.GetCategories(bson.M{}, 0, 1000)
	if err != nil {
		logger.Errorf("Error finding customer: %v ", err)
		AbortWithError(c, http.StatusInternalServerError, NewError(DataFetchingError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{"categories": categories})
}
