package controllers

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"bitbucket.org/boolow5/bolowdeliveryapi/models"
	"bitbucket.org/boolow5/bolowdeliveryapi/utils"
	"github.com/gin-gonic/gin"
)

var (
	successResponse = gin.H{
		"msg": "success",
	}
)

// Signup registers new user
func Signup(c *gin.Context) {
	logger.Debugf("Signup")
	// userID := bson.ObjectIdHex(c.MustGet("id").(string))
	input := struct {
		Username  string          `json:"username"`
		Email     string          `json:"email"`
		Phone     string          `json:"phone"`
		Password  string          `json:"password"`
		FirstName string          `json:"first_name"`
		LastName  string          `json:"last_name"`
		MidleName string          `json:"midle_name"`
		Gender    string          `json:"gender"`
		Birthday  time.Time       `json:"birthday"`
		IsVendor  bool            `json:"is_vendor"`
		Address   models.Location `json:"address"`
		ShopName  string          `json:"shop_name"`
	}{}
	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}
	missingFieldErrors := []error{}
	// check if already is registered
	_, err = models.GetUserByEmail(input.Email)
	emailExists := err == nil
	_, err = models.GetUserByPhone(input.Phone)
	phoneExists := err == nil
	if emailExists || phoneExists {
		var fields []string
		if emailExists {
			fields = append(fields, "email")
		}
		if phoneExists {
			fields = append(fields, "phone")
		}
		logger.Error("email is already registered")
		AbortWithError(c, http.StatusBadRequest, NewError(AlreadyExists, strings.Join(fields, ", ")))
		return
	}
	if len(input.Password) < 6 {
		logger.Error("password is too short: ", len(input.Password))
		missingFieldErrors = append(missingFieldErrors, []error{errors.New("password is too short")}...)
	}
	if len(input.FirstName) < 1 {
		logger.Error("first name is missing: ", input.FirstName)
		missingFieldErrors = append(missingFieldErrors, []error{errors.New("first name is missing")}...)
	}

	if len(missingFieldErrors) > 0 {
		utils.AbortWithErrors(c, http.StatusBadRequest, missingFieldErrors)
		return
	}
	isClean, phoneNumber := utils.CleanPhoneNumber(input.Phone)
	if isClean {
		input.Phone = phoneNumber
	}

	user := models.User{
		Username: input.Username,
		Email:    input.Email,
		Phone:    input.Phone,
		IsVendor: input.IsVendor,
		Profile: models.Profile{
			FirstName:  input.FirstName,
			MiddleName: input.MidleName,
			LastName:   input.LastName,
			Birthday:   input.Birthday,
			Gender:     input.Gender,
		},
	}
	user.SetPassword(input.Password)

	errs := models.Save(&user)
	if errs != nil && len(errs) > 0 {
		logger.Error("saving user failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	if input.IsVendor {
		shopName := input.ShopName
		if len(shopName) == 0 {
			shopName = user.Profile.FullName()
		}
		shop := models.Shop{
			Coords: input.Address,
			Name:   shopName,
		}
		errs = models.Save(&shop)
		if errs != nil && len(errs) > 0 {
			logger.Error("saving shop failed: ", errs)
			models.Remove(&user)
			AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
			return
		}
		vendor := models.Vendor{
			User:    user,
			Address: input.Address,
			Shops:   []models.Shop{shop},
		}
		errs = models.Save(&vendor)
		if errs != nil && len(errs) > 0 {
			logger.Error("saving vendor failed: ", errs)
			models.Remove(&user)
			models.Remove(&shop)
			AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
			return
		}
		shop.OwnerID = vendor.ID
		errs = models.Update(&shop)
		if errs != nil && len(errs) > 0 {
			logger.Error("Updating shop owner failed: ", errs)
			models.Remove(&user)
			AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
			return
		}
	} else {
		customer := models.Customer{
			User:    user,
			Address: input.Address,
		}
		errs = models.Save(&customer)
		if errs != nil && len(errs) > 0 {
			logger.Error("saving customer failed: ", errs)
			fmt.Printf("\n\nCUSTOMER: %+v\n\n", customer)
			models.Remove(&user)
			AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
			return
		}
	}
	newUser := models.User{}
	err = models.GetItemByID(user.ID, &newUser)
	if err != nil {
		logger.Error("finding user failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}
	token, err := newUser.GetToken()
	if err != nil {
		logger.Error("token generation failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(TokenGenerationError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": token, "level": user.Level, "is_vendor": input.IsVendor})
}

// Login registers new user
func Login(c *gin.Context) {
	logger.Debugf("Login")
	// userID := bson.ObjectIdHex(c.MustGet("id").(string))
	input := struct {
		Username string `json:"username"`
		Password string `json:"password"`
		IsVendor bool   `json:"is_vendor"`
	}{}
	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}
	missingFieldErrors := []error{}

	if len(input.Password) < 6 {
		logger.Error("password is too short: ", len(input.Password))
		missingFieldErrors = append(missingFieldErrors, []error{errors.New("password is too short")}...)
	}
	user := models.User{}
	err = models.GetItemByFilter(user.GetColName(), &bson.M{"username": input.Username}, &user)

	ok, err := user.Authenticate(input.Password)
	logger.Debugf("User Authenticate: is OK? %v, ERROR: %+v ", ok, err)
	if err != nil {
		logger.Error("login failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if !ok {
		logger.Error("login failed: not OK")
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, "Not OK"))
		return
	}
	token, err := user.GetToken()
	if err != nil {
		logger.Error("token generation failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(TokenGenerationError, err.Error()))
		return
	}

	if user.IsVendor && !input.IsVendor {
		err = fmt.Errorf("Please login as seller")
		logger.Error("seller login failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(TokenGenerationError, err.Error()))
		return
	}

	if !user.IsVendor && input.IsVendor {
		err = fmt.Errorf("Please login as customer")
		logger.Error("customer login failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(TokenGenerationError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"token":        token,
		"level":        user.Level,
		"is_vendor":    user.IsVendor,
		"logged_in_as": input.IsVendor,
	})
}

// GetProfile finds the current user's profile
func GetProfile(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"profile": user.Profile})
}

// GetCurrentUser finds the data for current user
func GetCurrentUser(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	user.Password = "[hidden]"
	c.JSON(http.StatusOK, gin.H{"user": user})
}

// GetUserLevel checks if current user's level
func GetUserLevel(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	switch user.Level {
	case models.GuestUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "guest"})
	case models.NormalUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "normal"})
	case models.AdminUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "admin"})
	}
}
