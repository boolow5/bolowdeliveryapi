package main

import (
	"fmt"

	"bitbucket.org/boolow5/bolowdeliveryapi/config"
	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"bitbucket.org/boolow5/bolowdeliveryapi/models"
	"bitbucket.org/boolow5/bolowdeliveryapi/notifications"
	"bitbucket.org/boolow5/bolowdeliveryapi/router"
)

func main() {
	conf := config.Get()
	logger.Init(logger.DebugLevel)
	models.Version = conf.Version
	fmt.Printf("Starting api with database %s...\n", conf.MgDBName)
	db.Init()
	err := models.Init()
	if err != nil {
		panic(err)
	}
	router := router.Init()
	go notifications.Start(conf.SSEPort)
	notifications.InitMailGun(conf.MailGun)
	router.Run(conf.Port)
}
