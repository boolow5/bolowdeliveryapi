package payments

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
)

// Transaction types
var (
	TrxTypePending = "pending"
	TrxTypeFailed  = "failed"
	TrxTypeSuccess = "success"
)

// Transaction is an object that tracks money movements between among the users and between the system and users
type Transaction struct {
	ID          uint64  `json:"id" bson:"_id" db:""`
	Description string  `json:"description" bson:"description" db:"null"`
	InitiatedBy uint64  `json:"initiated_by" bson:"initiated_by" db:"null"`
	Status      string  `json:"status" bson:"status" db:"default=pending"`
	Amount      float64 `json:"amount" bson:"amount" db:""`
	Currency    string  `json:"currency" bson:"currency" db:"default=pending"`
	UpdatedBy   uint64  `json:"updated_by" bson:"updated_by" db:"null"`
	CreatedAt   uint64  `json:"created_at" bson:"created_at" db:""`
	UpdatedAt   uint64  `json:"updated_at" bson:"updated_at" db:"null"`
}

// Wallet stores user's digital money in this system
type Wallet struct {
	ID        uint64  `json:"id" bson:"_id" db:""`
	OwnerID   uint64  `json:"owner_id" bson:"owner_id" db:"null"`
	Status    string  `json:"status" bson:"status" db:"default=pending"`
	Amount    float64 `json:"amount" bson:"amount" db:""`
	Currency  string  `json:"currency" bson:"currency" db:"default=pending"`
	CreatedAt uint64  `json:"created_at" bson:"created_at" db:""`
	UpdatedAt uint64  `json:"updated_at" bson:"updated_at" db:"null"`
}

// NewWallet initializes a wallet for a user
func NewWallet(ownerID uint64, currency string) Wallet {
	return Wallet{
		OwnerID:  ownerID,
		Currency: currency,
	}
}

// NewTransaction initializes a new transaction
func NewTransaction(amount float64, currency, description string, initiatedBy uint64) Transaction {
	return Transaction{
		Amount:      amount,
		Currency:    currency,
		Description: description,
		InitiatedBy: initiatedBy,
	}
}

// UpdateFromMap converts a map to Transaction object
func (t *Transaction) UpdateFromMap(m map[string]interface{}) (bool, error) {
	var ok, found bool
	// set this value temporarily until their is no error
	trx := t

	_, found = m["id"]
	if found {
		trx.ID, ok = m["id"].(uint64)
		if !ok {
			return ok, fmt.Errorf("id is not valid type")
		}
	}
	_, found = m["description"]
	if found {
		trx.Description, ok = m["description"].(string)
		if !ok {
			return ok, fmt.Errorf("description is not valid type")
		}
	}
	_, found = m["initiated_by"]
	if found {
		trx.InitiatedBy, ok = m["initiated_by"].(uint64)
		if !ok {
			return ok, fmt.Errorf("initiated_by is not valid type")
		}
	}
	_, found = m["status"]
	if found {
		trx.Status, ok = m["status"].(string)
		if !ok {
			return ok, fmt.Errorf("status is not valid type")
		}
	}
	_, found = m["amount"]
	if found {
		trx.Amount, ok = m["amount"].(float64)
		if !ok {
			return ok, fmt.Errorf("amount is not valid type")
		}
	}
	_, found = m["currency"]
	if found {
		trx.Currency, ok = m["currency"].(string)
		if !ok {
			return ok, fmt.Errorf("currency is not valid type")
		}
	}
	_, found = m["created_at"]
	if found {
		trx.CreatedAt, ok = m["created_at"].(uint64)
		if !ok {
			return ok, fmt.Errorf("created_at is not valid type")
		}
	}
	_, found = m["updated_at"]
	if found {
		trx.UpdatedAt, ok = m["updated_at"].(uint64)
		if !ok {
			return ok, fmt.Errorf("updated_at is not valid type")
		}
	}

	// all checks passed so this is safe to change
	t = trx
	return true, nil
}

// UpdateFromMap converts a map to Wallet object
func (w *Wallet) UpdateFromMap(m map[string]interface{}) (bool, error) {
	var ok, found bool
	// set this value temporarily until their is no error
	wallet := w

	_, found = m["id"]
	if found {
		wallet.ID, ok = m["id"].(uint64)
		if !ok {
			return ok, fmt.Errorf("id is not valid type")
		}
	}
	_, found = m["owner_id"]
	if found {
		wallet.OwnerID, ok = m["owner_id"].(uint64)
		if !ok {
			return ok, fmt.Errorf("owner_id is not valid type")
		}
	}
	_, found = m["status"]
	if found {
		wallet.Status, ok = m["status"].(string)
		if !ok {
			return ok, fmt.Errorf("status is not valid type")
		}
	}
	_, found = m["amount"]
	if found {
		wallet.Amount, ok = m["amount"].(float64)
		if !ok {
			return ok, fmt.Errorf("amount is not valid type")
		}
	}
	_, found = m["currency"]
	if found {
		wallet.Currency, ok = m["currency"].(string)
		if !ok {
			return ok, fmt.Errorf("currency is not valid type")
		}
	}
	_, found = m["created_at"]
	if found {
		wallet.CreatedAt, ok = m["created_at"].(uint64)
		if !ok {
			return ok, fmt.Errorf("created_at is not valid type")
		}
	}
	_, found = m["updated_at"]
	if found {
		wallet.UpdatedAt, ok = m["updated_at"].(uint64)
		if !ok {
			return ok, fmt.Errorf("updated_at is not valid type")
		}
	}

	// all checks passed so this is safe to change
	w = wallet
	return true, nil
}

// Topup moves amount from QR code to user's wallet
func Topup(amount float64, currency, cardNumber string) (bool, error) {
	description := fmt.Sprintf("Recharge %.2f %s from card number %s", amount, currency, cardNumber)
	trx := NewTransaction(amount, currency, description, 0)
	session := db.SQLTransaction()

	query, values := db.StructToTableInsert(&trx)

	result := session.MustExec(query, values...)

	id, err := result.LastInsertId()
	trx.ID = uint64(id)

	if err == nil && id > 0 {
		trx.Status = TrxTypeSuccess
		query, values = db.StructToTableUpdate(&trx)
		result = session.MustExec(query, values...)
		id, err := result.LastInsertId()
		return err == nil && id > 0, err
	}
	trx.Status = TrxTypeFailed
	query, values = db.StructToTableUpdate(&trx)
	result = session.MustExec(query, values...)
	id, err = result.LastInsertId()
	if err == nil && id > 0 {
		session.Commit()
	} else {
		session.Rollback()
	}
	return err == nil && id > 0, err
}

// Transfer transfers an amount from wallet to wallet
func Transfer(fromWalletID, toWalletID uint64, amount float64, currency string) (bool, error) {
	now := time.Now()
	description := fmt.Sprintf("Transfer %.2f %s from wallet %d to %d at %s", amount, currency, fromWalletID, toWalletID, now)

	trx := NewTransaction(amount, currency, description, 0)
	session := db.SQLTransaction()

	query, values := db.StructToTableInsert(&trx)
	result := session.MustExec(query, values...)

	id, err := result.LastInsertId()
	trx.ID = uint64(id)

	var fromWallet, toWallet Wallet

	fromWallet.ID = fromWalletID
	toWallet.ID = toWalletID

	if err == nil && id > 0 {
		err = db.GetTableDataByID(&fromWallet)
		if err != nil {

		}

		trx.Status = TrxTypeSuccess
		query, values = db.StructToTableUpdate(&trx)
		result = session.MustExec(query, values...)
		id, err := result.LastInsertId()
		return err == nil && id > 0, err
	}

	trx.Status = TrxTypeFailed
	query, values = db.StructToTableInsert(&trx)
	result = session.MustExec(query, values...)
	id, err = result.LastInsertId()

	if err == nil && id > 0 {
		session.Commit()
	} else {
		session.Rollback()
	}
	return err == nil && id > 0, err
}
