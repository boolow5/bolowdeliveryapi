package payments

import (
	"fmt"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
)

// Init prepare tables and other models
func Init() {
	fmt.Printf("Initializing payment API...")

	// regsiter tables
	err := db.Register(&Transaction{})
	if err != nil {
		panic(err)
	}
	err = db.Register(&Wallet{})
	if err != nil {
		panic(err)
	}
}
