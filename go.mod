module bitbucket.org/boolow5/bolowdeliveryapi

go 1.14

require (
	github.com/0xAX/notificator v0.0.0-20191016112426-3962a5ea8da1 // indirect
	github.com/GeertJohan/go.rice v1.0.0 // indirect
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/azer/debug v0.0.0-20141116004914-4769e572857f // indirect
	github.com/azer/go-style v0.0.0-20130627093536-14e31c5abbe5 // indirect
	github.com/codegangsta/envy v0.0.0-20141216192214-4b78388c8ce4 // indirect
	github.com/codegangsta/gin v0.0.0-20171026143024-cafe2ce98974 // indirect
	github.com/codingsince1985/geo-golang v1.6.1
	github.com/gin-gonic/gin v1.6.1
	github.com/jinzhu/gorm v1.9.12
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-shellwords v1.0.10 // indirect
	github.com/mroth/sseserver v1.1.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/skip2/go-qrcode v0.0.0-20191027152451-9434209cb086
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59
	gopkg.in/dgrijalva/jwt-go.v2 v2.7.0
	gopkg.in/mailgun/mailgun-go.v1 v1.1.1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/urfave/cli.v1 v1.20.0 // indirect
)
