package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"gopkg.in/mgo.v2/bson"
)

// Product represents an item that is sold by a Shop
type Product struct {
	ID                bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Shop              Shop          `json:"shop" bson:"shop"`
	Name              string        `json:"name" bson:"name"`
	Description       string        `json:"description" bson:"description"`
	Price             float64       `json:"price" bson:"price"`
	Tax               float64       `json:"tax" bson:"tax"`
	TotalPrice        float64       `json:"total_price" bson:"total_price"`
	AvailableQuantity int64         `json:"available_quantity" bson:"available_quantity"`
	OrderQuantity     int64         `json:"order_quantity" bson:"order_quantity"`
	Category          Category      `json:"category" bson:"category"`

	Timestamp `bson:",inline"`
}

// GetColName to satisfy DBObject interface
func (c Product) GetColName() string {
	return "product"
}

// GetID to satisfy DBObject interface
func (c Product) GetID() bson.ObjectId {
	return c.ID
}

// SetID to satisfy DBObject interface
func (c *Product) SetID(id bson.ObjectId) {
	c.ID = id
}

// SetTimestamp to satisfy DBObject interface
func (c *Product) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
	} else {
		c.UpdatedAt = now
	}
}

// IsValid to satisfy DBObject interface
func (c Product) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(c.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid Order ID")}
	}
	valid, errs := c.Shop.IsValid()
	return valid, errs
}

// AutoSetID to satisfy DBObject interface
func (c Product) AutoSetID() bool {
	return true
}

// GetProductsByID finds a product by id
func GetProductsByID(ids []bson.ObjectId) (items []Product, err error) {
	temp := Product{}
	err = db.FindAll(temp.GetColName(), &bson.M{"_id": bson.M{"$in": ids}}, &items)
	if err != nil {
		return items, err
	}
	return items, nil
}

// GetProductsByShopID finds a product by id
func GetProductsByShopID(shopID bson.ObjectId) (items []Product, err error) {
	temp := Product{}
	err = db.FindAll(temp.GetColName(), &bson.M{"shop._id": shopID}, &items)
	if err != nil {
		return items, err
	}
	return items, nil
}

// GetProductsByCategoryName finds a product by name
func GetProductsByCategoryName(categoryName string) (items []Product, err error) {
	temp := Product{}
	err = db.FindAll(temp.GetColName(), &bson.M{
		"$or": []bson.M{
			{"category.name": bson.RegEx{Pattern: categoryName, Options: "i"}},
			{"category.parent.name": bson.RegEx{Pattern: categoryName, Options: "i"}},
		},
	}, &items)
	if err != nil {
		return items, err
	}
	return items, nil
}

// GetProductByID finds a product by id
func GetProductByID(id bson.ObjectId) (item Product, err error) {
	err = db.FindOne(item.GetColName(), &bson.M{"_id": id}, &item)
	if err != nil {
		return item, err
	}
	return item, nil
}
