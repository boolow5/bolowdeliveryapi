package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"gopkg.in/mgo.v2/bson"
)

// Vendor represents a shop owner or seller
type Vendor struct {
	User `bson:",inline"`

	Address Location `json:"address" bson:"address"`
	Shops   []Shop   `json:"shops" bson:"shops"`
}

// GetColName to satisfy DBObject interface
func (c Vendor) GetColName() string {
	return "vendor"
}

// GetID to satisfy DBObject interface
func (c Vendor) GetID() bson.ObjectId {
	return c.ID
}

// SetID to satisfy DBObject interface
func (c *Vendor) SetID(id bson.ObjectId) {
	c.ID = id
}

// SetTimestamp to satisfy DBObject interface
func (c *Vendor) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
	} else {
		c.UpdatedAt = now
	}
}

// IsValid to satisfy DBObject interface
func (c Vendor) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(c.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid Vendor ID")}
	}
	return true, nil
}

// AutoSetID to satisfy DBObject interface
func (c Vendor) AutoSetID() bool {
	return true
}

// GetVendorByUserID finds a user by his/her id
func GetVendorByUserID(id bson.ObjectId) (user Vendor, err error) {
	logger.Debugf("GetVendorByID(id: %s)", id)
	err = db.FindOne(user.GetColName(), &bson.M{"profile._id": id}, &user)
	if err != nil {
		return user, err
	}
	return user, nil
}

// GetVendorByID finds a user by his/her id
func GetVendorByID(id bson.ObjectId) (user Vendor, err error) {
	logger.Debugf("GetVendorByID(id: %s)", id)
	err = db.FindOne(user.GetColName(), &bson.M{"_id": id}, &user)
	if err != nil {
		return user, err
	}
	return user, nil
}
