package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"gopkg.in/mgo.v2/bson"
)

// Order statuses
const (
	OrderStatusPending    = "pending"
	OrderStatusVerified   = "verified"
	OrderStatusProcessing = "processing"
	OrderStatusDelivering = "delivering"
	OrderStatusReceived   = "received"
	OrderStatusCancelled  = "cancelled"
	OrderStatusRejected   = "rejected"
)

// Order represents a request from a customer for the delivery of items
type Order struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Status   string        `json:"status" bson:"status"`
	Items    []Product     `json:"items" bson:"items"`
	Customer Customer      `json:"customer" bson:"customer"`

	Timestamp `bson:",inline"`
}

// GetColName to satisfy DBObject interface
func (c Order) GetColName() string {
	return "order"
}

// GetID to satisfy DBObject interface
func (c Order) GetID() bson.ObjectId {
	return c.ID
}

// SetID to satisfy DBObject interface
func (c *Order) SetID(id bson.ObjectId) {
	c.ID = id
}

// SetTimestamp to satisfy DBObject interface
func (c *Order) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
	} else {
		c.UpdatedAt = now
	}
}

// IsValid to satisfy DBObject interface
func (c Order) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(c.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid Order ID")}
	}
	if len(c.Items) > 0 {
		return true, nil
	}
	return false, []error{fmt.Errorf("order without items is invalid")}
}

// AutoSetID to satisfy DBObject interface
func (c Order) AutoSetID() bool {
	return true
}

// Notify creates notification about the changes of the order
func (c Order) Notify(action, msg, targetURL string, initiatedBy Profile, targetUsers []bson.ObjectId) error {
	if action == "" {
		action = "inform"
	}
	if !bson.IsObjectIdHex(initiatedBy.UserID.Hex()) {
		return fmt.Errorf("Invalid initiator user ID")
	}

	notification := Notification{
		InitiatedBy:     initiatedBy,
		Message:         msg,
		ForAllCustomers: false,
		ForAllVendors:   false,
		TargetUsers:     targetUsers,
		ReadUsers:       []bson.ObjectId{},
		TargetURL:       targetURL,
	}

	errs := Save(&notification)
	if errs != nil && len(errs) > 0 {
		return fmt.Errorf("%v", errs)
	}

	return nil
}

// GetOrdersByUserID finds orders by customer's id
func GetOrdersByUserID(id bson.ObjectId) (orders []Order, err error) {
	logger.Debugf("GetOrdersByUserID(id: %s)", id)
	temp := Order{}
	err = db.FindAllSort(temp.GetColName(), &bson.M{"customer.profile._id": id}, &orders, "-created_at")
	if err != nil {
		return orders, err
	}
	return orders, nil
}
