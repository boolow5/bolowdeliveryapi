package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"gopkg.in/mgo.v2/bson"
)

// Category represents a family of products
type Category struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name        string        `json:"name" bson:"name"`
	Description string        `json:"description" bson:"description"`
	ImageURL    string        `json:"image_url" bson:"image_url"`
	Parent      *Category     `json:"parent" bson:"parent"`

	Timestamp `bson:",inline"`
}

// GetColName to satisfy DBObject interface
func (c Category) GetColName() string {
	return "category"
}

// GetID to satisfy DBObject interface
func (c Category) GetID() bson.ObjectId {
	return c.ID
}

// SetID to satisfy DBObject interface
func (c *Category) SetID(id bson.ObjectId) {
	c.ID = id
}

// SetTimestamp to satisfy DBObject interface
func (c *Category) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
	} else {
		c.UpdatedAt = now
	}
}

// IsValid to satisfy DBObject interface
func (c Category) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(c.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid Category ID")}
	}
	if c.Parent != nil {
		return c.Parent.IsValid()
	}
	return true, nil
}

// AutoSetID to satisfy DBObject interface
func (c Category) AutoSetID() bool {
	return true
}

// GetCategories finds all categories
func GetCategories(filter bson.M, page, limit int) (categories []Category, err error) {
	temp := Category{}
	err = db.FindPageWithLimit(temp.GetColName(), &filter, page, limit, &categories)
	return categories, err
}

// GetCategoryByName finds all categories
func GetCategoryByName(name string) (category Category, err error) {
	err = db.FindOne(category.GetColName(), &bson.M{"name": name}, &category)
	return category, err
}

// InitCategories loads initial categories if they don't exist
func InitCategories() error {
	file, err := ioutil.ReadFile(filepath.Join("data", "categories.json"))
	if err != nil {
		return err
	}
	categories := []Category{}
	err = json.Unmarshal(file, &categories)
	if err != nil {
		return err
	}
	var errs []error
	for _, category := range categories {
		if len(category.Name) == 0 {
			continue // skip this round
		}
		if count, err := db.Count(category.GetColName(), &bson.M{"name": category.Name}); err == nil && count == 0 {
			// create new one
			if category.Parent != nil {
				err = db.FindOne(category.GetColName(), &bson.M{
					"name": category.Parent.Name,
				}, category.Parent)
				if err != nil {
					fmt.Printf("\n[InitCategories] saving '%s' as parent of '%s' category failed: %+v\n", category.Parent.Name, category.Name, errs)
				}
			}
			errs = Save(&category)
			if errs != nil && len(errs) > 0 {
				fmt.Printf("\n[InitCategories] saving '%s' category failed: %+v\n", category.Name, errs)
			}
		}
	}
	return nil
}
