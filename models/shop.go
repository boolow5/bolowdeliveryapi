package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"gopkg.in/mgo.v2/bson"
)

// Shop represents a store or a vendor
type Shop struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	OwnerID   bson.ObjectId `json:"owner_id" bson:"owner_id,omitempty"`
	Coords    Location      `json:"coords" bson:"coords"`
	Name      string        `json:"name" bson:"name"`
	Score     float64       `json:"score" bson:"score"`
	ItemsSold int64         `json:"items_sold" bson:"items_sold"`

	Timestamp `bson:",inline"`
}

// GetColName to satisfy DBObject interface
func (c Shop) GetColName() string {
	return "shop"
}

// GetID to satisfy DBObject interface
func (c Shop) GetID() bson.ObjectId {
	return c.ID
}

// SetID to satisfy DBObject interface
func (c *Shop) SetID(id bson.ObjectId) {
	c.ID = id
}

// SetTimestamp to satisfy DBObject interface
func (c *Shop) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
	} else {
		c.UpdatedAt = now
	}
}

// IsValid to satisfy DBObject interface
func (c Shop) IsValid() (bool, []error) {
	if c.Coords.Long == 0 {
		return false, []error{fmt.Errorf("invalid location latidude")}
	}
	if c.Coords.Lat == 0 {
		return false, []error{fmt.Errorf("invalid location longitude")}
	}
	if len(c.Name) == 0 {
		return false, []error{fmt.Errorf("invalid Shop name")}
	}
	return true, nil
}

// AutoSetID to satisfy DBObject interface
func (c Shop) AutoSetID() bool {
	return true
}

// GetOwners gets the owners of the shop
func (c Shop) GetOwners() (owners []Vendor, err error) {
	logger.Debugf("GetOwners shop id: %s\n", c.ID)
	temp := Vendor{}
	err = db.FindAll(temp.GetColName(), &bson.M{"shops.id": c.ID}, &owners)
	return owners, err
}

// GetShopByID finds a shop by his/her id
func GetShopByID(id bson.ObjectId) (shop Shop, err error) {
	logger.Debugf("GetShopByID(id: %s)", id)
	err = db.FindOne(shop.GetColName(), &bson.M{"_id": id}, &shop)
	if err != nil {
		return shop, err
	}
	return shop, nil
}

// UpdateShop updates shop info
func UpdateShop(shopID, vendorID bson.ObjectId, newData Shop) error {
	vendor, err := GetVendorByUserID(vendorID)
	if err != nil {
		return err
	}
	shop, err := GetShopByID(shopID)
	if err != nil {
		return err
	}
	shop.Coords = newData.Coords
	shop.Name = newData.Name

	found := false

	for i := range vendor.Shops {
		if vendor.Shops[i].ID == shopID {
			vendor.Shops[i] = shop
			errs := Update(&vendor)
			if errs != nil && len(errs) > 0 {
				return fmt.Errorf("%v", errs)
			}
			found = true
		}
	}
	if found {
		errs := Update(&shop)
		if errs != nil && len(errs) > 0 {
			return fmt.Errorf("%v", errs)
		}
		return nil
	}

	return fmt.Errorf("Access denied: shop belongs to another vendor")
}

// GetNearbyShops finds shops in radius of kilometers
func GetNearbyShops(location Location, categories []Category, name string, radius float64) (shops []Shop, err error) {
	temp := Shop{}
	err = db.FindAll(temp.GetColName(), &bson.M{
		"coords": bson.M{
			"$near": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": []float64{location.Long, location.Lat},
				},
				"$maxDistance": radius * 1000, // precission in kilometers
			},
		},
	}, &shops)
	return shops, err
}
