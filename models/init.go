package models

import (
	"fmt"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
)

// Init initializes models
func Init() error {
	fmt.Println("*************** Models Init ***************")
	// initialize database indices
	createIndexes()
	err := InitCategories()
	if err != nil {
		fmt.Printf("[InitCategories] returned error: %v\n", err)
		return err
	}
	return nil
}

func createIndexes() {
	logger.Debug("*************** Adding indexes ***************")
	// err := db.AddUniqueIndex(UserCollection, "email", "phone")
	// if err != nil {
	// 	logger.Errorf("Failed to add unique fields 'email,phone' to '%s' collection. Error: %v", UserCollection, err)
	// }
	// err = db.Add2DIndex(DistrictCollection, "$2d:center")
	// if err != nil {
	// 	logger.Errorf("Failed to add 2d index 'center' to '%s' collection. Error: %v", DistrictCollection, err)
	// }

	user := User{}
	logger.Info(user.GetColName(), ": adding unique index [username], \t\t\nERROR: ", db.AddUniqueIndex(user.GetColName(), "username"), "\n")
	logger.Info(user.GetColName(), ": adding unique index [email], \t\t\nERROR: ", db.AddUniqueIndex(user.GetColName(), "email"), "\n")

	shop := Shop{}
	logger.Error(shop.GetColName(), ": adding 2D index [$2d:coords], \nERROR: ", db.Add2DIndex(shop.GetColName(), "$2d:coords"), "\n")
	logger.Error(shop.GetColName(), ": adding 2D index [$2dsphere:coords], \nERROR: ", db.Add2DIndex(shop.GetColName(), "$2dsphere:coords"), "\n")

	customer := Customer{}
	logger.Error(customer.GetColName(), ": adding 2D index [$2d:address], \nERROR: ", db.Add2DIndex(customer.GetColName(), "$2d:address"), "\n")
	logger.Error(customer.GetColName(), ": adding 2D index [$2dsphere:address], \nERROR: ", db.Add2DIndex(customer.GetColName(), "$2dsphere:address"), "\n")
}
