package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"gopkg.in/mgo.v2/bson"
)

// Notification represents a message which is delivered for each user
type Notification struct {
	ID              bson.ObjectId   `json:"id" bson:"_id,omitempty"`
	Status          string          `json:"status" bson:"status"`
	InitiatedBy     Profile         `json:"initiated_by" bson:"initiated_by"`
	Message         string          `json:"message" bson:"message"`
	ForAllCustomers bool            `json:"for_all_customers" bson:"for_all_customers"`
	ForAllVendors   bool            `json:"for_all_vendors" bson:"for_all_vendors"`
	TargetUsers     []bson.ObjectId `json:"target_users" bson:"target_users"`
	ReadUsers       []bson.ObjectId `json:"read_users" bson:"read_users"`
	TargetURL       string          `json:"target_url" bson:"target_url"`

	Timestamp `bson:",inline"`
}

// GetColName to satisfy DBObject interface
func (c Notification) GetColName() string {
	return "notification"
}

// GetID to satisfy DBObject interface
func (c Notification) GetID() bson.ObjectId {
	return c.ID
}

// SetID to satisfy DBObject interface
func (c *Notification) SetID(id bson.ObjectId) {
	c.ID = id
	if c.TargetURL == "" {
		c.TargetURL = "/"
	}
}

// SetTimestamp to satisfy DBObject interface
func (c *Notification) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
		if c.TargetURL == "" {
			c.TargetURL = "/"
		}
	} else {
		c.UpdatedAt = now
	}
}

// IsValid to satisfy DBObject interface
func (c Notification) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(c.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid Notification ID")}
	}
	if len(c.Message) == 0 {
		return false, []error{fmt.Errorf(("Empty message"))}
	}
	return true, nil
}

// AutoSetID to satisfy DBObject interface
func (c Notification) AutoSetID() bool {
	return true
}

// GetNotificationsByUserID finds notifications by customer's id
func GetNotificationsByUserID(id bson.ObjectId) (notifications []Notification, err error) {
	logger.Debugf("GetNotificationsByUserID(id: %s)", id)
	temp := Notification{}
	err = db.FindAllSort(temp.GetColName(), &bson.M{"customer.profile._id": id}, &notifications, "-created_at")
	if err != nil {
		return notifications, err
	}
	return notifications, nil
}
