package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"gopkg.in/mgo.v2/bson"
)

// Customer represents a shopper
type Customer struct {
	User `bson:",inline"`

	Address Location `json:"address" bson:"address"`
}

// GetColName to satisfy DBObject interface
func (c Customer) GetColName() string {
	return "customer"
}

// GetID to satisfy DBObject interface
func (c Customer) GetID() bson.ObjectId {
	return c.ID
}

// SetID to satisfy DBObject interface
func (c *Customer) SetID(id bson.ObjectId) {
	c.ID = id
}

// SetTimestamp to satisfy DBObject interface
func (c *Customer) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
	} else {
		c.UpdatedAt = now
	}
}

// IsValid to satisfy DBObject interface
func (c Customer) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(c.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid Customer ID")}
	}
	return true, nil
}

// AutoSetID to satisfy DBObject interface
func (c Customer) AutoSetID() bool {
	return true
}

// GetCustomerByID finds a user by his/her id
func GetCustomerByID(id bson.ObjectId) (user Customer, err error) {
	logger.Debugf("GetCustomerByID(id: %s)", id)
	err = db.FindOne(CustomerCollection, &bson.M{"profile._id": id}, &user)
	if err != nil {
		return user, err
	}
	return user, nil
}
