package models

import (
	"gopkg.in/mgo.v2/bson"
)

// Country represents a country
type Country struct {
	Name string `json:"name" bson:"name"`
	Code string `json:"code" bson:"code"`
}

// Location a coordinates and more info for that location
type Location struct {
	ID      bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Long    float64       `json:"lng" bson:"lng"`
	Lat     float64       `json:"lat" bson:"lat"`
	Street  string        `json:"street" bson:"street"`
	County  string        `json:"county" bson:"county"`
	City    string        `json:"city" bson:"city"`
	Country Country       `json:"country" bson:"country"`
}
