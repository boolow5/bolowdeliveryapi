package models

const (
	// UserCollection is the name of user database collection name
	UserCollection = "user"
	// CustomerCollection is the name of customer user
	CustomerCollection = "customer"
	// LocationCollection is the name of location database collection name
	LocationCollection = "location"
	// DriverCollection is the name of driver
	DriverCollection = "driver"
	// VehicleCollection is the name of vehicle
	VehicleCollection = "vehicle"
	// CurrencyCollection is the name of currency
	CurrencyCollection = "currency"
	// TripCollection is the name of trip
	TripCollection = "trip"
	// UserReviewCollection is the name of user_review
	UserReviewCollection = "user_review"
	// UserRatingCollection is the name of user_rating
	UserRatingCollection = "user_rating"
	// DistrictCollection is the name for district
	DistrictCollection = "district"
	// WalletCollection is the name for wallet collection
	WalletCollection = "wallet"
	// TransactionCollection is the name for transaction collection
	TransactionCollection = "transaction"
	// StaticFileCollection is the name for static files collection
	StaticFileCollection = "static_file"
	// SettingsCollection is the name of settings collection
	SettingsCollection = "settings"
)
