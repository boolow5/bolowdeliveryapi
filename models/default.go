package models

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/boolow5/bolowdeliveryapi/db"
	"bitbucket.org/boolow5/bolowdeliveryapi/logger"
	"gopkg.in/mgo.v2/bson"
)

var (
	// Version of the last releases
	Version string
	// CommitNo last git commit number
	CommitNo string
	// LastBuidDate the last deployment date
	LastBuidDate string
)

// DBObject defines the behavior of object that can be saved to database
type DBObject interface {
	GetColName() string
	GetID() bson.ObjectId
	SetID(bson.ObjectId)
	SetTimestamp(bool)
	IsValid() (bool, []error)
	AutoSetID() bool
}

// Timestamp is an object to be embedded in other objects for tracking date time
type Timestamp struct {
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

func capitalize(str string) string {
	return strings.Title(strings.ToLower(str))
}

// Save stores an item in the database
func Save(item DBObject) []error {
	if item.AutoSetID() {
		item.SetID(bson.NewObjectId())
	}
	logger.Debugf("Save(item: %s) ID: [%s]", item.GetColName(), item.GetID().Hex())
	if valid, errs := item.IsValid(); errs != nil || !valid {
		logger.Error("Errors:", errs)
		return append([]error{fmt.Errorf("%s is not valid", item.GetColName())}, errs...)
	}
	item.SetTimestamp(true)
	err := db.CreateStrong(item.GetColName(), item)
	if err != nil {
		return []error{err}
	}
	return nil
}

// Update stores changes to an item in the database
func Update(item DBObject) []error {
	item.SetTimestamp(false)
	if valid, errs := item.IsValid(); errs != nil || !valid {
		return append([]error{fmt.Errorf("%s is not valid", item.GetColName())}, errs...)
	}
	err := db.UpdateStrong(item.GetColName(), item.GetID(), item)
	if db.IsNotFound(err) {
		return []error{fmt.Errorf("%s %s", capitalize(item.GetColName()), err)}
	}
	if err != nil {
		return []error{err}
	}
	return nil
}

// UpdateAll stores changes to items in the database
func UpdateAll(items ...DBObject) (errs []error) {
	for i := 0; i < len(items); i++ {
		items[i].SetTimestamp(false)
		if valid, errs := items[i].IsValid(); errs != nil || !valid {
			errs = append([]error{fmt.Errorf("%s is not valid", items[i].GetColName())}, errs...)
		}
	}

	if errs != nil && len(errs) > 0 {
		return errs
	}

	for i := 0; i < len(items); i++ {
		err := db.UpdateStrong(items[i].GetColName(), items[i].GetID(), items[i])
		if db.IsNotFound(err) {
			errs = append(errs, fmt.Errorf("%s %s", capitalize(items[i].GetColName()), err))
		}
	}

	if errs != nil && len(errs) > 0 {
		return errs
	}
	return nil
}

// Remove deletes an item from the database
func Remove(item DBObject) error {
	err := db.DeleteId(item.GetColName(), item.GetID())
	if db.IsNotFound(err) {
		return fmt.Errorf("%s %s", capitalize(item.GetColName()), err)
	}
	return err
}

// GetItemByID finds DBObject item by it's ID
func GetItemByID(id bson.ObjectId, output DBObject) error {
	err := db.FindOne(output.GetColName(), &bson.M{"_id": id}, output)
	if db.IsNotFound(err) {
		return fmt.Errorf("%s %s", capitalize(output.GetColName()), err)
	}
	return err
}

// GetItemsByFilter finds DBObject items by it's ID
func GetItemsByFilter(colName string, filter *bson.M, output []DBObject) error {
	err := db.FindAll(colName, filter, output)
	if db.IsNotFound(err) {
		return fmt.Errorf("%s %s", capitalize(colName), err)
	}
	return err
}

// GetItemByFilter finds DBObject item by it's ID
func GetItemByFilter(colName string, filter *bson.M, output DBObject) error {
	err := db.FindOne(colName, filter, output)
	if db.IsNotFound(err) {
		return fmt.Errorf("%s %s", capitalize(colName), err)
	}
	return err
}
